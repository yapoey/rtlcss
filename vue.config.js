const WebpackRTLPlugin = require('webpack-rtl-plugin')
module.exports = {
    // initialize scss variables for the whole project
    css: {
        extract: {
            filename: 'css/[name].css',
            chunkFilename: 'css/[name].css',
        },
    },
    configureWebpack: {
        plugins: [
            new WebpackRTLPlugin()
        ]
    }
}
